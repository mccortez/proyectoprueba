package com.gbc.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication (exclude = {SecurityAutoConfiguration.class })
@ComponentScan("com.gbc.test")
@Configuration
public class UserRegisterApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserRegisterApplication.class, args);
	}

}
