package com.gbc.test.api;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gbc.test.adapter.bdd.datastore.UserDS;
import com.gbc.test.core.usecase.ManageUserUC;
import com.gbc.test.model.User;


@RestController
@RequestMapping("/user")
public class RegisterUserController {
	
	
	@Autowired
	UserDS userDS;
	
	@Autowired
	ManageUserUC manageUserUC;
	
	@PostMapping(path = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> saveUser(@Valid @RequestBody User user){
	    User savedUser =userDS.saveUser(user);
		
		return new ResponseEntity(savedUser, HttpStatus.OK);
	}
	
	
	@GetMapping(path = "/getAll", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<User>> getAllUser(){
		List<User> users =userDS.getAllUsers();
		
		return new ResponseEntity(users, HttpStatus.OK);
	}
	
	@GetMapping(path = "/getUserById", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<User> findUserByID(@Valid @RequestBody User user){
		
		User foundUser = manageUserUC.getUserById(user.getId());
		return new ResponseEntity(foundUser, HttpStatus.OK);
	}

}
