package com.gbc.test.model;



import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
	private Long id;

	private String name;

	private String email;

	private Date birth;
}
