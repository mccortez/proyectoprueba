package com.gbc.test.adapter.bdd.datastore;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gbc.test.adapter.bdd.entity.UserEntity;
import com.gbc.test.adapter.bdd.repository.UserRepository;
import com.gbc.test.model.User;

@Component
public class UserDSImpl implements UserDS{

	@Autowired
	UserRepository userRepository;
	
	public User saveUser(User newUser) {
		UserEntity savedEntity = userRepository.save(getUserEntity(newUser));
		
		return getUser(savedEntity);
	}
	
	public List<User> getAllUsers(){
		return getListUser(userRepository.findAll());
	}
	
	private UserEntity getUserEntity(User user) {
		UserEntity newEntity = new UserEntity();
		newEntity.setId(user.getId());
		newEntity.setName(user.getName());
		newEntity.setEmail(user.getEmail());
		newEntity.setBirth(user.getBirth());
		return newEntity;
	}
	
	private User getUser(UserEntity entity) {
		User newUser = new User();
		newUser.setId(entity.getId());
		newUser.setName(entity.getName());
		newUser.setEmail(entity.getEmail());
		newUser.setBirth(entity.getBirth());
		return newUser;
	}
	
	private List<User> getListUser(Iterable<UserEntity> entity){
		List<User> listUsers = new ArrayList<User>();
		
		for (UserEntity userEntity : entity) {
			listUsers.add(getUser(userEntity));
			
		}
		return listUsers;
	}
	
	
}
