package com.gbc.test.adapter.bdd.repository;

import org.springframework.data.repository.CrudRepository;

import com.gbc.test.adapter.bdd.entity.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Long> {

}
