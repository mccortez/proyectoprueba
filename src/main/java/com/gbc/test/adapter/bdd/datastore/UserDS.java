package com.gbc.test.adapter.bdd.datastore;

import java.util.List;

import com.gbc.test.adapter.bdd.entity.UserEntity;
import com.gbc.test.model.User;

public interface UserDS {
	
	public User saveUser(User newUser);
	
	public List<User> getAllUsers();

}
