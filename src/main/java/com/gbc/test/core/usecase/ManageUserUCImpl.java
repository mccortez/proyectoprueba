package com.gbc.test.core.usecase;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gbc.test.adapter.bdd.datastore.UserDS;
import com.gbc.test.model.User;


public class ManageUserUCImpl implements ManageUserUC{
	
	@Autowired
	UserDS 	userDS;
	
	public User getUserById(Long Id) {
		List<User> listResult = userDS.getAllUsers();
		
		for (User user : listResult) {
		    if(Id.equals(user.getId())) {
		    	return user;
		    }
		}
		return null;
	}


}
