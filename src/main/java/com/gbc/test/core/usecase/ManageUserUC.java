package com.gbc.test.core.usecase;

import com.gbc.test.model.User;

public interface ManageUserUC {
	
	public User getUserById(Long Id);

}
